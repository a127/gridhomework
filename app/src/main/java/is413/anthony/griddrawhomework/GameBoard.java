package is413.anthony.griddrawhomework;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anthony on 4/16/2015.
 */
public class GameBoard extends View
{
    private Paint background = new Paint();
    private Paint stroke = new Paint();
    private Paint cell = new Paint();

    private int cell_size = 50;

    protected static int[][] box;

    public GameBoard(Context context)
    {
        super(context);

    }

    public GameBoard(Context c, AttributeSet attributeSet)
    {
        super(c, attributeSet);


    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        background.setColor(Color.BLACK);
        background.setStyle(Paint.Style.STROKE);

        canvas.drawRect(0, 0, 100, 100, background);

        stroke.setColor(Color.RED);
        stroke.setStrokeWidth(cell_size);
        stroke.setStyle(Paint.Style.FILL);

        cell.setColor(Color.GREEN);
        cell.setStyle(Paint.Style.STROKE);

        int height = getMeasuredHeight() / cell_size;
        int width = getMeasuredWidth() / cell_size;

        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if (MainActivity.game_box[j][i] != 0)
                {
                    canvas.drawRect(
                            j * cell_size,
                            i * cell_size,
                            (j * cell_size) + cell_size,
                            (i * cell_size) + cell_size, stroke);
                }
                else
                {

                    canvas.drawRect(
                            j * cell_size,
                            i * cell_size,
                            (j * cell_size) + cell_size,
                            (i * cell_size) + cell_size, cell);

                }
            }
        }

    }
}
