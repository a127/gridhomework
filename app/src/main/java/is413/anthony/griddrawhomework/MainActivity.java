package is413.anthony.griddrawhomework;

import android.graphics.Point;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity
{

    private GameBoard game_board;

    // Get custom height and width;
    private static final int HEIGHT = 1300;
    private static final int WIDTH = 1900;
    int cell_size = 50;

    private List<Point> points = new ArrayList<>();
    protected static int[][] game_box = new int[HEIGHT][WIDTH];

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        game_board = (GameBoard) findViewById(R.id.game_board);

        game_board.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {

                int touch_x = (int) event.getX();
                int touch_y = (int) event.getY();
                try
                {
                    game_box[touch_x / cell_size][touch_y / cell_size] = 1;

                    v.invalidate();
                } catch (ArrayIndexOutOfBoundsException aioobe)
                {

                    return false;

                }

                return true;
            }
        });

    }

}
